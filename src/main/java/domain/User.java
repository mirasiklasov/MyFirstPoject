package domain;

import java.util.Date;

public class User {
    private long id;
    private String name;
    private String surname;
    private String password;
    private Date birthday;

    public User(){

    }

    public User(String name, String surname, String password, Date birthday){
        setBirthday(birthday);
        setName(name);
        setSurname(surname);
        setPassword(password);

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }
}
