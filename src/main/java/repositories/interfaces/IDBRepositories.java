package repositories.interfaces;

import java.sql.Connection;
public interface IDBRepositories {
    Connection getConnection();
}
